# ENTREGA CONVOCATORIA JUNIO
Pablo Clarke Alvarez 

**Correo universidad:** p.clarke.2018@alumnos.urjc.es

**Video explicativo** : https://www.youtube.com/watch?v=0n2yK2LfsGg

## Parte básica

### Comentarios sobre la parte básica
La parte básica del proyecto incluye la implementación de tres componentes principales: `signaling.py`, `streamer.py` , `front.py`y `index.html` que se sirve desde `front.py`

1. **signaling.py**: python3 signaling.py <signal_port>
   - Este servidor de señalización recibe mensajes UDP de los servidores de streaming y del frontend web.
   - Almacena la información de los streamers registrados y gestiona la señalización necesaria para establecer conexiones WebRTC.
   - **Comentario**: El servidor de señalización ha sido probado y funciona correctamente según los requisitos especificados.

2. **streamer.py**: python3 streamer.py <file> <signal_ip> <signal_port>
   - Envía un mensaje de registro al servidor de señalización al arrancar junto con el registro del video.
   - Maneja ofertas SDP para iniciar conexiones WebRTC y envía la respuesta correspondiente.
   - Envía el fichero de video especificado a través de la conexión WebRTC.
   - **Comentario**: El servidor de streaming maneja múltiples conexiones simultáneas y funciona correctamente.

3. **front.py**: python3 front.py <http_port> <signal_ip> <signal_port>
   - Solicita la lista de videos al servidor de señalización al arrancar.
   - Sirve una página HTML ('index.html') donde los usuarios pueden seleccionar y ver videos.
   - Gestiona la señalización WebRTC entre el navegador y el servidor de streaming.
   - **Comentario**: El servidor web muestra correctamente la lista de videos y maneja las conexiones WebRTC según lo esperado.
   - 
4. **index.html**
El archivo `index.html` sirve como interfaz de usuario para seleccionar y visualizar los videos disponibles. Aquí hay una explicación de su estructura y funcionalidades principales:

- **Estructura HTML**: 
  - Contiene un título, un menú desplegable para seleccionar el video, y botones para iniciar y detener la transmisión.
  - Incluye un elemento `video` para mostrar el video seleccionado.

- **Estilos CSS**:
  - Define estilos básicos para el cuerpo, el título, y los contenedores para mejorar la presentación visual.

- **JavaScript**:
  - Maneja la lógica para cargar la lista de videos desde el servidor, iniciar la transmisión de video a través de WebRTC y detener la transmisión.
  - `loadVideoList()`: Se ejecuta al cargar la página y solicita la lista de videos disponibles al servidor frontal.
  - `startStreaming()`: Inicia la conexión WebRTC y solicita el video seleccionado al servidor de señalización.
   - `stopStreaming()`: Detiene la transmisión de video y cierra la conexión WebRTC.

# Funcionalidades arregladas:
- Al refrescar la página web, se puede volver a reproducir los videos sin tener que cerrar los servidores de fornt, signaling y streamer:
- --Para ello se ha implementado localStorage en la función startStreaming(propiedad del pbjeto window de JavaScript que permite almacenar datos de manera persistente), para
- --guardar el archivo de video seleccionado y restaurar esta información cuando se refresque la página.
- Al utilizar la funcionalidad de stop se puede volver a reproducir los videos vistos. Cerrando la conexión WebRTC

## Parte adicional

### Opciones realizadas
* Varios servidores frontales
    - El sistema admite múltiples servidores frontales. Los navegadores pueden utilizar cualquier servidor frontal disponible.
    - Para probar esta funcionalidad, se deben lanzar varios servidores frontales con distinto puerto (python3 front.py <http_port> <signal_ip> <signal_port>) y visualizar los distintos videos cargados desde el streamer

* Información adicional para los ficheros
    - Los streamers envían información adicional (título, desccripción) al servidor de señalización durante el registro.
    - Para su implementación, he creado dos diccionarios uno con los titulos de los videos y otro con la descripción de estos mismos que a la hora de hacer el registro de los videos, se envían.
    - Esta información se muestra en la página HTML del frontend.
    - Para probar esta funcionalidad, se deben registrar streamers con distinto video






