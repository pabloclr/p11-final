import asyncio
import logging
import cv2
import json
import fractions
from aiortc import RTCPeerConnection, RTCSessionDescription
from aiortc.contrib.media import MediaStreamTrack, MediaPlayer
from aiortc.mediastreams import VideoFrame

description = {
    "video.mp4": "Este video del nuevo Porsche 911 GT3 RS de 525CV muestra las capacidades sobre pista de este modelo.",
    "video2.mp4": "Este video es un breve resumen de un minuto de lo que ocurrio en Enero en el rally de montecarlo de 2020",
    "video3.mp4": "Video del 60 aniversario del Porsche 911 s/t"
}
title = {
    "video.mp4": "Porsche 911 GT3 RS",
    "video2.mp4": "WRC 2020 Monaco",
    "video3.mp4": "60 years of the Porsche 911"
}

logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s %(levelname)s %(message)s',
    handlers=[
        logging.FileHandler("log_entry.txt"),
        logging.StreamHandler()
    ]
)

class VideoStreamTrack(MediaStreamTrack):
    kind = "video"

    def __init__(self, file, width=640, height=480):
        super().__init__()
        self.cap = cv2.VideoCapture(file)
        self.frame_time = 1 / self.cap.get(cv2.CAP_PROP_FPS)
        self.start_time = None
        self.width = width
        self.height = height
        if not self.cap.isOpened():
            logging.error(f"No se pudo abrir el archivo de video: {file}")

    async def recv(self):
        if self.start_time is None:
            self.start_time = asyncio.get_event_loop().time()
            self.pts = 0

        # Calcular el tiempo transcurrido y ajustar el frame
        pts, time_base = self.next_timestamp()

        ret, frame = self.cap.read()
        if not ret:
            self.cap.set(cv2.CAP_PROP_POS_FRAMES, 0)
            ret, frame = self.cap.read()

        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        frame = cv2.resize(frame, (self.width, self.height))
        frame = VideoFrame.from_ndarray(frame, format="rgb24")

        # Incrementar pts por el tiempo de un frame
        self.pts += self.frame_time
        frame.pts = pts
        frame.time_base = time_base

        # Calcular el tiempo de dormir para mantener la tasa de cuadros por segundo
        elapsed = asyncio.get_event_loop().time() - self.start_time
        sleep_time = self.pts - elapsed
        if sleep_time > 0:
            await asyncio.sleep(sleep_time)

        return frame

    def next_timestamp(self):
        """
        Genera una nueva marca de tiempo para el frame.
        """
        frame_time = self.frame_time
        pts = int(self.cap.get(cv2.CAP_PROP_POS_FRAMES) * frame_time * 1000)
        time_base = fractions.Fraction(1, 1000)
        return pts, time_base

class StreamerProtocol:
    def __init__(self, file, title, description):
        self.file = file
        self.title = title
        self.description = description
        self.peers = []

    def connection_made(self, transport):
        self.transport = transport
        logging.info("Conexión establecida")

    def datagram_received(self, data, addr):
        message = data.decode()
        logging.info(f"Mensaje recibido de {addr}: {message}")

        try:
            message = json.loads(message)
        except json.JSONDecodeError:
            logging.error("Error decodificando el mensaje")
            return

        if message["type"] == "OFFER":
            offer = RTCSessionDescription(sdp=message["sdp"], type='offer')
            logging.info(f"Oferta SDP recibida: {offer.sdp}")
            asyncio.ensure_future(self.handle_offer(offer, addr))

    async def handle_offer(self, offer, addr):
        pc = RTCPeerConnection()
        self.peers.append(pc)

        @pc.on('icecandidate')
        async def on_icecandidate(candidate):
            if candidate:
                logging.info(f"New ICE candidate: {candidate}")

        @pc.on('iceconnectionstatechange')
        async def on_iceconnectionstatechange():
            logging.info(f"ICE connection state is {pc.iceConnectionState}")
            if pc.iceConnectionState == 'failed':
                await pc.close()
                self.peers.remove(pc)

        await pc.setRemoteDescription(offer)
        logging.info("Descripción remota configurada")

        media_player = MediaPlayer(self.file)
        video_track = VideoStreamTrack(self.file)
        pc.addTrack(video_track)
        pc.addTrack(media_player.audio)
        logging.info("Pistas de medios añadidas al RTCPeerConnection")

        answer = await pc.createAnswer()
        logging.info("Respuesta SDP creada")

        await pc.setLocalDescription(answer)
        logging.info("Descripción local configurada")

        response = json.dumps(
            {"type": "ANSWER", "sdp": pc.localDescription.sdp}).encode()
        self.transport.sendto(response, addr)
        logging.info(f"Respuesta SDP enviada: {response}")

async def main(file, signal_ip, signal_port):
    logging.info("Iniciando servidor de streaming")

    loop = asyncio.get_event_loop()
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: StreamerProtocol(file, title[file], description[file]),
        local_addr=('0.0.0.0', 0)
    )

    register_message = json.dumps(
        {"type": "REGISTER",
         "file": file,
         "title": title[file],
         "description": description[file]}).encode()
    transport.sendto(register_message, (signal_ip, signal_port))
    logging.info(f"Registro enviado a {signal_ip}:{signal_port}")

    try:
        await asyncio.sleep(3600)  # Mantener el servidor corriendo
    except KeyboardInterrupt:
        pass

if __name__ == '__main__':
    import sys

    file = sys.argv[1]
    signal_ip = sys.argv[2]
    signal_port = int(sys.argv[3])
    asyncio.run(main(file, signal_ip, signal_port))
